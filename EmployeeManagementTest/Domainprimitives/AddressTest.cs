using EmployeeManagement.Domainprimitives;
using System.Diagnostics.CodeAnalysis;

namespace EmployeeManagementTest.Domainprimitives;

[TestClass]
public class AddressTest
{
    [TestClass]
    public class EqualityTests
    {
        private Address a12a, a12b, a13, a12j;

        [TestInitialize]
        public void Setup()
        {
            a12a = new Address("A12", 2, "Grevenbroich");
            a12b = new Address("A12", 2, "Grevenbroich");
            a13 = new Address("A13", 2, "Grevenbroich");
            a12j = new Address("A12", 2, "J�lich");
        }

        [TestMethod]
        public void TestEqual()
        {
            // arrange
            // act
            // assert
            Assert.AreEqual(a12a, a12b);
        }

        [TestMethod]
        public void TestNotEqual()
        {
            // arrange
            // act
            // assert
            Assert.AreNotEqual(a12a, a13);
            Assert.AreNotEqual(a12a, a12j);
        }


        [TestMethod]
        public void TestUpperFloor()
        {
            // arrange
            Address aUp = a13.OneFloorUp();

            // act
            // assert
            Assert.AreEqual(a13.Floor+1, aUp.Floor);
            Assert.AreEqual(a13.Campus, aUp.Campus);
            Assert.AreNotEqual(a13, aUp);
        }


        [TestMethod]
        public void TestLowerFloor()
        {
            // arrange
            Address aDown = a13.OneFloorDown();

            // act
            // assert
            Assert.AreEqual(a13.Floor-1, aDown.Floor);
            Assert.AreEqual(a13.Building, aDown.Building);
            Assert.AreNotEqual(a13, aDown);
        }


        [TestMethod]
        public void TestLowerFloorNotLowerThanGround()
        {
            // arrange
            Address a = new Address("B2", 0, "K�ln");

            // act
            // assert
            Assert.ThrowsException<EmployeeManagementException>(() => a.OneFloorDown());
        }

    }

}